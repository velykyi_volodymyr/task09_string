package com.velykyi.strings;

import com.velykyi.strings.view.View;

public class App {
    public static void main(String[] args) {
        View view = new View();
        view.showMenu();
    }
}
