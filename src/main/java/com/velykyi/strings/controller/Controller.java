package com.velykyi.strings.controller;

import com.velykyi.strings.model.Model;
import com.velykyi.strings.model.StringUtils;

public class Controller {
    Model utils = new StringUtils();

    public void addNumber(int num){
        utils.addNumber(num);
    }

    public void addString(String str){
        utils.addString(str);
    }

    public String getString(){
        return utils.concatParam();
    }
}
