package com.velykyi.strings.model;

public interface Model {
    String concatParam();
    void addNumber(int num);
    void addString(String str);
}
