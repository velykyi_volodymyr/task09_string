package com.velykyi.strings.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class StringUtils implements Model {
    private static Logger logger1 = LogManager.getLogger(StringUtils.class);
    private List<String> strList;
    private List<Integer> intList;

    public StringUtils(List<String> strList, List<Integer> intList) {
        this.strList = strList;
        this.intList = intList;
    }

    public StringUtils() {
        this.strList = new ArrayList<>();
        this.intList = new ArrayList<>();
    }

    @Override
    public String concatParam() {
        StringBuilder str = new StringBuilder();
        for (String el : this.strList) {
            str.append(el);
        }
        for (Integer el : this.intList) {
            str.append(el);
        }
        logger1.info("Concatenation of parameters was done.");
        return str.toString();
    }

    public void addNumber(int num) {
        intList.add(num);
        logger1.info("Number was added.");
    }

    public void addString(String str){
        strList.add(str);
        logger1.info("String was added.");
    }
}
