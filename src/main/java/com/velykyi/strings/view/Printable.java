package com.velykyi.strings.view;

@FunctionalInterface
public interface Printable {
    void print();
}
