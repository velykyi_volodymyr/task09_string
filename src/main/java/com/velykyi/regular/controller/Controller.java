package com.velykyi.regular.controller;

import com.velykyi.regular.model.Model;
import com.velykyi.regular.model.Sentence;

public class Controller {
    private Model model;

    public void addSentence(String str){
        this.model = new Sentence(str);
    }

    public void checkSentence(){
        this.model.check();
    }

    public String replaceVowels(){
        return this.model.replace();
    }
}
