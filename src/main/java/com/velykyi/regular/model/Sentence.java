package com.velykyi.regular.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence implements Model {
    String sentence;

    public Sentence(String sentence) {
        this.sentence = sentence;
    }

    @Override
    public void check() {
        Pattern pattern = Pattern.compile("[A-Z][^.]*\\.");
        Matcher matcher = pattern.matcher(this.sentence);
        while (matcher.find()) {
            System.out.println(sentence.substring(matcher.start(), matcher.end()));
        }
    }

    @Override
    public String replace() {
        String vowels = "AaEeYyUuIiOo";
        String pattern = "[" + vowels + "]";
        return this.sentence.replaceAll(pattern, "_");
    }
}
