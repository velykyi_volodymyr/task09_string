package com.velykyi.regular.model;

public interface Model {
    void check();
    String replace();
}
