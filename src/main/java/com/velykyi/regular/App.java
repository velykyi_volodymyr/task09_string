package com.velykyi.regular;

import com.velykyi.regular.view.View;

public class App {
    public static void main(String[] args) {
        View view = new View();
        view.showMenu();
    }
}
