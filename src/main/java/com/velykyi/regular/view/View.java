package com.velykyi.regular.view;

import com.velykyi.regular.controller.Controller;
import java.util.*;

public class View {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    Controller controller = new Controller();
    private static Scanner scann = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("q", bundle.getString("q"));
    }

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menuRegular", locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::addSentenceAndCheck);
        methodMenu.put("2", this::addSentebceAndReplace);
        methodMenu.put("3", this::setEnglish);
        methodMenu.put("4", this::setUkrainian);
        methodMenu.put("5", this::setGerman);
        methodMenu.put("6", this::setGreek);

    }

    private void addSentebceAndReplace() {
        controller.addSentence(scann.nextLine());
        System.out.println(controller.replaceVowels());
    }

    private void addSentenceAndCheck() {
        controller.addSentence(scann.nextLine());
        controller.checkSentence();
    }

    private void setEnglish(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("menuRegular", locale);
        setMenu();
    }

    private void setUkrainian(){
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menuRegular", locale);
        setMenu();
    }

    private void setGerman(){
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("menuRegular", locale);
        setMenu();
    }

    private void setGreek(){
        locale = new Locale("el");
        bundle = ResourceBundle.getBundle("menuRegular", locale);
        setMenu();
    }



    public void showMenu() {
        String keyMenu;
        do {
            System.out.println("\nMENU:");
            for (String str : menu.values()) {
                System.out.println(str);
            }
            System.out.println("Please, select menu point.");
            keyMenu = scann.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.toUpperCase().equals("Q"));
    }
}
