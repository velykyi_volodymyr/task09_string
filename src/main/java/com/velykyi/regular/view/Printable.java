package com.velykyi.regular.view;

@FunctionalInterface
public interface Printable {
    void print();
}
